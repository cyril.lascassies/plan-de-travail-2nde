\begin{enumerate}
  \item Montrez que la droite $(OF)$est parallèle à la droite $(BM)$. Celle-ci est perpendiculaire à la droite $[AB]$.
  \item $(OF)$ passe par le milieu de $[AB]$ et est perpendiculaire à $(AB)$, donc ...
  \item \begin{enumerate}
          \item Le centre de gravité du triangle $ABM$ est le point concours des médianes.
          \item  $ABM$ est un triangle rectangle. On sait où se situe le centre de son cercle circonscrit.
        \end{enumerate}
        \item Utilisez une question précédente.
  \item La droite est perpendiculaire au diamètre en un un point du cercle. C'est une droite particulière pour le cercle. On l'appelle ...
\end{enumerate}
