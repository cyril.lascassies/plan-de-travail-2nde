\begin{enumerate}
\item Utilisez la réciproque du théorème de Pythagore.
\item Montrez que c'est un parallélogramme, puis que c'est un rectangle.
\item \begin{enumerate}
\item $PR=3,75$ cm
\item Aire $=33,75$ cm$^2$
\end{enumerate}
\end{enumerate}
