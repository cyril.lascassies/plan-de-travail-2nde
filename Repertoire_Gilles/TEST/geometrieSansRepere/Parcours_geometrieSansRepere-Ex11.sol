\begin{enumerate}
\item
\begin{enumerate}
\item Utilisez le théorème de Pythagore.
\item $10\sqrt{2}=\sqrt{200}$
\item Calculez $DE^2$ en utilisant le théorème de Pythagore.
\end{enumerate}
\item Recommencez les calculs précédents en posant  $AB=a$.
\item Utilisez la question précédente.
\end{enumerate}

