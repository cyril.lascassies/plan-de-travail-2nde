\begin{enumerate}
\item Le centre de ce cercle est un point défini dans l'énoncé. N'oubliez pas que les deux triangles sont équilatéraux.
\item Utilisez le cercle défini précédemment. Il s'agit du cercle circonscrit au triangle $ABE$.
\item Il y a deux longueurs à calculer : $AE$ et $BE$ (avec le théorème de Pythagore). On trouve $BE=\sqrt{45}$.
\item $BCD$ est un triangle isocèle (prouvez-le !) avec un angle de 60° (prouvez-le !).
\end{enumerate}


