Calculez les quantités de sucre dans chacune des deux boissons. Puis donnez la proportion de sucre dans le mélange.\\
Il y a $26,25$ \% de sucre dans le mélange.
