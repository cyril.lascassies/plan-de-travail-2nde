La population de référence est celle des élèves du lycée.\\
              La sous-population est celle des élèves de première et d'après l'énoncé, $p_1=39\,\%$.\\
              Dans cette sous-population, on note $p_2$ la proportion des élèves en première technologique.\\
              La proportion $P$ des élèves en première technologique parmi les élèves du lycée est $P=7{,}02\,\%$.

\medskip
 D'après le cours, on a $P=p_1\times p_2$, ce qui donne  $0{,}070\,2=0{,}39\times p_2$\\
              Ainsi, $p_2=\dfrac{0{,}070\,2}{0{,}39}=0{,}18$.\\
              Il y a donc $18\,\%$ des élèves de première en première technologique.
