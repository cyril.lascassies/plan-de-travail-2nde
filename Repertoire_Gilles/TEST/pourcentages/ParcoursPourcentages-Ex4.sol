\begin{enumerate}[itemsep=2em]
\item Pour appliquer une proportion à une valeur, on multiplie celle-ci par la proportion $p$. \\Comme il y a  $20\,\%$ des $450$ salariés qui sont cadres, le nombre de cadres est donné par :\\$\dfrac{20}{100} \times 450 = 0{,}2 \times 450=90$\\Il y a donc  $90$  cadres dans cette entreprise.
\item La proportion $p$ est donnée par le quotient : $\dfrac{216}{2\,700} = 0{,}08$.\\$0{,}08=\dfrac{8}{100}$. Le pourcentage de bruants des roseaux dans la réserve est donc de $8~\%$.
\item Soit $x$ le nombre total de spectateur. \\ Comme $76~\%$ de $x$ est égal à $304$, on a :\\$\begin{aligned}
              \dfrac{76}{100} \times x &= 304 \\\
              0{,}76 \times x &= 304 \\
              x &= \dfrac{304}{0{,}76} \\
              x &= 400
              \end{aligned}$\\Il y avait donc $400$ spectateurs.
\end{enumerate}
