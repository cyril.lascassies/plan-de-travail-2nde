\begin{enumerate}
\item Calculer $p\,\%$ d'un nombre, c'est multiplier ce nombre par $\dfrac{p}{100}$.
\\    Ainsi, $10\,\%$  de $18$ est égal à $0{,}1\times 18=1{,}8$.
\item Calculer $p\,\%$ d'un nombre, c'est multiplier ce nombre par $\dfrac{p}{100}$.
\\    Ainsi, $61\,\%$  de $72$ est égal à $0{,}61\times 72=43{,}92$.
\item Calculer $p\,\%$ d'un nombre, c'est multiplier ce nombre par $\dfrac{p}{100}$.
\\    Ainsi, $3\,\%$  de $79$ est égal à $0{,}03\times 79=2{,}37$.
\end{enumerate}
