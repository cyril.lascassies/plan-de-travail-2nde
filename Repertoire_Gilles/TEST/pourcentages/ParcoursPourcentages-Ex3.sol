\begin{enumerate}[itemsep=1em]
\item Calculer la fraction d'un nombre, c'est multiplier la fraction par ce nombre.

    Ainsi, $\dfrac{3}{5}$  de $30$ est égal à $\dfrac{3}{5}\times 30=\dfrac{3\times30}{5}=\dfrac{3\times5\times6}{5}=18$.
\item Calculer la fraction d'un nombre, c'est multiplier la fraction par ce nombre.



    Ainsi, $\dfrac{4}{11}$  de $99$ est égal à $\dfrac{4}{11}\times 99=\dfrac{4\times99}{11}=\dfrac{4\times11\times9}{11}=36$.
\item Calculer la fraction d'un nombre, c'est multiplier la fraction par ce nombre.



    Ainsi, $\dfrac{9}{7}$  de $35$ est égal à $\dfrac{9}{7}\times 35=\dfrac{9\times35}{7}=\dfrac{9\times7\times5}{7}=45$.
\end{enumerate}
