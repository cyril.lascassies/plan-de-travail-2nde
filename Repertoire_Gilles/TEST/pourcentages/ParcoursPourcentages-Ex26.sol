\begin{enumerate}
\item Si une grandeur subit des évolutions successives, le coefficient multiplicateur global est le produit des coefficients multiplicateurs de chaque évolution.

\medskip
\textbf{Première évolution :} \\ Diminuer de $16~\%$ revient à multiplier par $CM_1 = 1 - \dfrac{16}{100} = 0{,}84$.

\medskip
\textbf{Évolution globale :} \\
           Diminuer de $17{,}68~\%$ revient à multiplier par $CM = 1 - \dfrac{17{,}68}{100} = 0{,}823\,2$.

\medskip
En notant $CM_2$ le coefficient multiplicateur de la deuxième évolution, on a : $CM = CM_1 \times CM_2$, soit
             $0{,}84 \times CM_2 =0{,}823\,2$ et par suite $ CM_2 =\dfrac{0{,}823\,2}{0{,}84 }=0{,}98$

\medskip
Or $CM_2 = 0{,}98 = 1 - 0{,}02 = 1-\dfrac{2}{100}$ ce qui correspond à une baisse de $2~\%$.\\En $2022$, le nombre d'habitants de cette ville a baissé de $2\,\%$.
\item Si une grandeur subit des évolutions successives, le coefficient multiplicateur global est le produit des coefficients multiplicateurs de chaque évolution.

\medskip
\textbf{Première évolution :} \\ Augmenter de $40~\%$ revient à multiplier par $CM_1 = 1 + \dfrac{40}{100} = 1{,}4$.

\medskip
\textbf{Évolution globale :} \\
          Augmenter de $3{,}6~\%$ revient à multiplier par $CM = 1 + \dfrac{3{,}6}{100} = 1{,}036$.

\medskip
En notant $CM_2$ le coefficient multiplicateur de la deuxième évolution, on a : $CM = CM_1 \times CM_2$, soit
             $1{,}4 \times CM_2 =1{,}036$ et par suite $ CM_2 =\dfrac{1{,}036}{1{,}4 }=0{,}74$

\medskip
Or $CM_2 = 0{,}74 = 1 - 0{,}26 = 1-\dfrac{26}{100}$ ce qui correspond à une baisse de $26~\%$.\\Le nombre d'adhérents de cette association a baissé de $26\,\%$ en $2022$.
\end{enumerate}
