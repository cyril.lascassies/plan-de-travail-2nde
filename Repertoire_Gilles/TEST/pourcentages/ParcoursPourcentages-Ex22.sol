\begin{enumerate}
\item On utilise la formule du cours qui exprime le taux d'évolution $t$ en fonction de la valeur initiale $V_i$ et la valeur finale $V_f$: $t=\dfrac{V_f-V_i}{V_i}$.

\medskip
Ici : $t=\dfrac{73{,}70-55}{55}=0{,}34=\dfrac{34}{100}$.\\Le prix a donc augmenté de $34~\%$.\\Méthode 2 : On arrive aussi au même résultat en passant par le coefficient multiplicateur égal à $\dfrac{V_f}{V_i}$ :

\medskip
$\dfrac{73{,}70}{55} = 1{,}34 =  1 + 0{,}34 = 1 + \dfrac{34}{100}$.
\item On utilise la formule du cours qui exprime le taux d'évolution $t$ en fonction de la valeur initiale $V_i$ et la valeur finale $V_f$: $t=\dfrac{V_f-V_i}{V_i}$.

\medskip
Ici : $t=\dfrac{24\,360-21\,000}{21\,000}=0{,}16=\dfrac{16}{100}$.\\La population a donc augmenté de $16~\%$.\\Méthode 2 : On arrive aussi au même résultat en passant par le coefficient multiplicateur égal à $\dfrac{V_f}{V_i}$ :

\medskip
$\dfrac{24\,360}{21\,000} = 1{,}16 =  1 + 0{,}16 = 1 + \dfrac{16}{100}$.
\end{enumerate}
