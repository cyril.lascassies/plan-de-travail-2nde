\begin{enumerate}
  \item $p=\dfrac{13}{22}$.
  \item
  \begin{enumerate}
   \item $f\simeq 30,3$ \%.
   \item $f\simeq 10,6$ \%.
  \end{enumerate}
  \item Pourcentage de cadres parmi les hommes : environ $15,2$ \%.\\
  Pourcentage de cadres parmi les femmes : environ $17,6$ \%.\\
 Chez les femmes il y a plus de cadres que chez les hommes.
 \end{enumerate}
