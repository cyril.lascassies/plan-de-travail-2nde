\begin{enumerate}[itemsep=1em]
\item Pour déterminer le taux d'évolution global, on commence par calculer le coefficient multiplicateur global.\\Si une grandeur subit des évolutions successives, le coefficient multiplicateur global est le produit des coefficients multiplicateurs de chaque évolution :

\medskip
\textbf{Première évolution :} \\
           Diminuer de $16~\%$ revient à multiplier par $CM_1 = 1 - \dfrac{16}{100} = 0{,}84$.

\medskip
\textbf{Deuxième évolution :} \\
           Diminuer de $2~\%$ revient à multiplier par $CM_2 = 1 - \dfrac{2}{100} = 0{,}98$.

\medskip
Le coefficient multiplicateur global est égal à $CM = CM_1 \times CM_2 = 0{,}84 \times 0{,}98 =0{,}823\,2$.

\medskip
Or $CM = 0{,}823\,2 = 1 - 0{,}176\,8 = 1-\dfrac{17{,}68}{100}$ ce qui correspond à une baisse de $17{,}68~\%$.\\Le nombre d'habitants de cette ville a diminué de $17{,}68~\%$ entre $2021$ et $2022$.
\item Pour déterminer le taux d'évolution global, on commence par calculer le coefficient multiplicateur global.\\Si une grandeur subit des évolutions successives, le coefficient multiplicateur global est le produit des coefficients multiplicateurs de chaque évolution :

\medskip
\textbf{Première évolution :} \\ Augmenter de $40~\%$ revient à multiplier par $CM_1 = 1 + \dfrac{40}{100} = 1{,}4$.

\medskip
\textbf{Deuxième évolution :}\\
           Diminuer de $26~\%$ revient à multiplier par $CM_2 = 1 - \dfrac{26}{100} = 0{,}74$.

\medskip
Le coefficient multiplicateur global est égal à $CM = CM_1 \times CM_2 = 1{,}4 \times 0{,}74 =1{,}036$.

\medskip
Or $CM =1{,}036 = 1 + 0{,}036 = 1 + \dfrac{3{,}6}{100}$ ce qui correspond à une hausse de $3{,}6~\%$.\\Le nombre d'adhérents de cette association a augmenté de $3{,}6~\%$ entre $2020$ et $2022$.
\end{enumerate}
