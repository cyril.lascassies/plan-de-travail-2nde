\begin{enumerate}
\item Multiplier par $1{,}2$ revient à augmenter de $20~\%$ car $1{,}2 = 120~\% = 100~\% + 20~\%$.
\item Diminuer de $80~\%$ revient à multiplier par $0{,}2$ car $100~\% - 80~\% = 20~\%$.
\item Augmenter de $60~\%$ revient à multiplier par $1{,}6$ car $100~\% + 60~\% = 160~\%$.
\item Multiplier par $0{,}98$ revient à diminuer de $2~\%$ car $0{,}98 = 98~\% = 100~\% - 2~\%$.
\end{enumerate}
