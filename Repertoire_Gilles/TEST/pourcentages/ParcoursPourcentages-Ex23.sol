\begin{enumerate}[itemsep=1em]
\item Augmenter de $34~\%$ revient à multiplier par $1 + \dfrac{34}{100} = 1+ 0{,}34 = 1{,}34$.\\Pour retrouver le prix initial, on va donc diviser le prix final par $1{,}34$.\\$\dfrac{73{,}70}{1{,}34}  = 55$\\Avant l'augmentation cet article coûtait $55$ €.
\item Augmenter de $16~\%$ revient à multiplier par $1 + \dfrac{16}{100} = 1+ 0{,}16 = 1{,}16$.\\Pour retrouver la population initiale, on va donc diviser le nombre d'habitants actuel par $1{,}16$.\\$\dfrac{24\,360}{1{,}16}  = 21\,000$\\Il y a 13 ans cette ville comptait $21\,000$ habitants.
\end{enumerate}
