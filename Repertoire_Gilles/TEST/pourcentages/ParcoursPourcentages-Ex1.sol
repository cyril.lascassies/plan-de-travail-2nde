\begin{enumerate}
\item $\dfrac{1}{2}=0{,}5=50\,\%$
\item $62\,\%=0{,}62=\dfrac{62}{100}$
\item $0{,}003\,4=\dfrac{0{,}34}{100}=0{,}34 \,\%$
\item $0{,}89=\dfrac{89}{100}=89 \,\%$
\end{enumerate}
