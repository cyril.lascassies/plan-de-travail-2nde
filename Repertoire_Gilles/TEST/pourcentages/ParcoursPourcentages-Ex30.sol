\begin{enumerate}
\item Pour déterminer le taux d'évolution réciproque, on commence par calculer le coefficient multiplicateur associé :\\Diminuer de $19\,\%$ revient à multiplier par $ 1 - \dfrac{19}{100} = 0{,}81$

\medskip
Le coefficient multiplicateur réciproque est donc : $\dfrac{1}{0{,}81} \approx 1{,}234\,6$.{\color[HTML]{f15929}\\Remarque : Il faut arrondir les valeurs à $10^{-4}$ pour avoir un arrondi en pourcentage à $10^{-2}$.}

\medskip
Or $1{,}234\,6 = 1 + 0{,}234\,6 = 1 + \dfrac{23{,}46}{100}$ ce qui correspond à une hausse de $23{,}46\,\%$.

\medskip
Il faut donc appliquer une hausse d'environ $23{,}46\,\%$ pour revenir au prix initial.
\item Pour déterminer le taux d'évolution réciproque, on commence par calculer le coefficient multiplicateur associé :\\Augmenter de $34~\%$ revient à multiplier par $ 1 + \dfrac{34}{100} = 1{,}34$

\medskip
Le coefficient multiplicateur réciproque est donc : $\dfrac{1}{1{,}34} \approx 0{,}746\,3$.{\color[HTML]{f15929}\\Remarque : Il faut arrondir les valeurs à $10^{-4}$ pour avoir un arrondi en pourcentage à $10^{-2}$.}

\medskip
Or $0{,}746\,3 = 1 - 0{,}253\,7 = 1 - \dfrac{25{,}37}{100}$ ce qui correspond à une baisse de $25{,}37\,\%$.

\medskip
Il faut donc appliquer une baisse d'environ $25{,}37\,\%$ pour revenir au niveau de départ.
\end{enumerate}
