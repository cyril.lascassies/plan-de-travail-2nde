\begin{enumerate}
\item
On a $\nombre{6000} \times 0,43 = \nombre{2580}$~(langues).
\item $\nombre{2349}$ langues  en voie de disparition.
\item
$\nombre{0,0385}$ soit 3,85\,\% pourcentage de langues éteintes.
\end{enumerate}
