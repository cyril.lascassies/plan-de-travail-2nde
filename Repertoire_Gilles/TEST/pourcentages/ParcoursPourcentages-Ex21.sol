\begin{enumerate}
\item Augmenter de $34~\%$ revient à multiplier par $1 + \dfrac{34}{100} = 1+ 0{,}34 = 1{,}34$.\\$55\times 1{,}34 = 73{,}70$\\Le nouveau prix de cet article est $73{,}70$ €.
\item Augmenter de $16~\%$ revient à multiplier par $1 + \dfrac{16}{100} = 1+ 0{,}16 = 1{,}16$.\\$21\,000\times 1{,}16 = 24\,360$\\La population de cette ville est maintenant de $24\,360$ habitants.
\end{enumerate}
