 Souligner le facteur commun puis écrire la factorisation.
 \begin{enumerate}
 \item $A = (2x-3)(2x+2)$
 \item $M=(15x+7)(-13x-2)$
 \item $U=3x(3x+2)$
 \item $S=(13x+5)(3x-13)$
  \item $E = x(8x-3)$
 \end{enumerate}
 
