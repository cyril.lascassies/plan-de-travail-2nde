 En mettant en évidence une différence de deux carrés, factoriser les expressions suivantes.
 \begin{enumerate}
  \item $H=(x-11)(x+11)$
  \item $E = (x - 10)(x+2)$
  \item $R = (x-\sqrt{5})(x+\sqrt{5})$
  \item $T = (3+x)(7-x) $
 \end{enumerate}
 
