

\begin{enumerate}
\item
\begin{enumerate}
\item Suivez le programme.
\item $31$
\end{enumerate}
\item Le résultat de A est $(x-2)^2 +4x$. On y est presque ....
\item $x^2+6$
\item
\begin{enumerate}
\item Vrai. Faites le calcul.
\item Faux. Trouvez un contre-exemple, c'est-à-dire un nombre entier à entrer dans le programme B qui donne un entier pair en sortie.
\item Vrai. Utilisez la forme littérale du résultat en sortie de ce programme pour justifier.
\item Vrai. Le carré d'un entier pair est un entier pair. De même le carré d'un entier impair est un entier impair. Et quand on ajoute un entier pair, que se passe-t-il ?
\end{enumerate}
\end{enumerate}



