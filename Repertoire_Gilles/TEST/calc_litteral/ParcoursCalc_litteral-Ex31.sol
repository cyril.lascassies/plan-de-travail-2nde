 On considère une fonction  $f$  définie sur $\mathbb{R}$ par :\\
  \hspace*{2cm}$f(x)=-2x^2-4x-5$
 \begin{enumerate}
  \item Développer $-2(x+1)^2-3$.
  \item Calculer les images suivantes.
  \begin{enumerate}
   \item $f(-1)=-3$
   \item $f\left(-\sqrt{3}\right)=-4\sqrt{3}-11$
   \item $f(0)=-5$
  \end{enumerate}
 \end{enumerate}
