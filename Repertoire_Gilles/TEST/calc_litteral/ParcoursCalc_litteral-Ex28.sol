\begin{enumerate}
\item Vérifier ce résultat sur quelques valeurs de $x$ ne suffit pas. \\
Il faut le prouver pour toutes les valeurs de $x$ réelles.\\
On développe l'expression et on doit trouver $3$.

 \item Développer $C$ et $D$ séparément et comparer les expression obtenues.
\end{enumerate}

  
