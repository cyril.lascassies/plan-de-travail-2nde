\begin{enumerate}
  \item L'aire, en fonction de $x$ est : $4x^2+4x+1$.
  \item $BC^2=2x^2+6x+5$
  \item Il existe une valeur de $x$ pour laquelle $ABC$ est rectangle en $A$ : $x=1$.
      \item L'aire augmente de $4x+4$ cm$^2$ et non de $4x$ cm$^2$. L'élève a donc tort.
          \item La longueur du côté du carré initial est 2 m.
\end{enumerate}
