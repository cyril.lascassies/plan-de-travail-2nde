 Soit $f$ la fonction définie sur $\mathbb{R}$ par : $$f(x)=2x^2-4x-30$$
 \begin{enumerate}
     \item $(2x+6)(x-5)=\ldots= f(x)$.
     \item  $(2x+2)(x-3)-24=\ldots =f(x)$.
    \end{enumerate}

  
