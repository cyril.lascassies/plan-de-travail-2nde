 \begin{enumerate}
  \item \begin{enumerate}
          \item $\nombre{1575}=3^2\times 5^2\times 7$
          \item $\sqrt{\nombre{1575}}=15\sqrt{7}$.
        \end{enumerate}
  \item  $\dfrac{\nombre{1575}}{\nombre{6615}}=\dfrac{5}{21}$.
\end{enumerate}
