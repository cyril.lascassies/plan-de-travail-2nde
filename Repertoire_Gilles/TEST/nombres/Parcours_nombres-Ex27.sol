\begin{enumerate}
  \item
    \begin{description}
    \item $720$ secondes pour retrouver simultanément le 1\ier\ et le 2\ieme\ signal.
    \item $450$ secondes pour retrouver simultanément le 2\ieme\ et le 3\ieme\ signal.
    \item $1\,200$ secondes pour retrouver simultanément le 1\ier\ et le 3\ieme\ signal.
    \end{description}
  \item  Les trois signaux sont émis simultanément toutes les heures.
\end{enumerate}
