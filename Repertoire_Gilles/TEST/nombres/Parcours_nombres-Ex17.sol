Soient $n$ et $n'$ les deux multiples de $7$.\\
 Traduire par une égalité que $n$ est un multiple de $7$. Faire de même pour $n'$.\\
 Écrire la somme de ces deux multiples sous une certaine forme pour obtenir le résultat.
