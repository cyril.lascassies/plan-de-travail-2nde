Soient $n$ et $n'$ les deux multiples de $a$.\\
 Traduire par une égalité que $n$ est un multiple de $a$. Faire de même pour $n'$.\\
 Écrire la somme de ces deux multiples sous une certaine forme pour obtenir le résultat.
