\begin{enumerate}
\item $\mathscr{D}_f=\mathscr{D}_g=[-5,5~;~5]$.
   \item $f(-2)=2$ et $f(4)=0$.
     \item Antécédents de 3 par $g$ : $-3$ et 2 .
     \item Antécédents de 1 par $f$ : $-5$, $-3$, $-1$ et $4,5$.
       \item $\mathscr{S}_1=\{-4~;~0~;~4\}$ \\
$\mathscr{S}_2=\{0,5~;~3\}$\\
$\mathscr{S}_3=\{-4~;~4,5\}$
       \item $\mathscr{S}_4=]0~;~4[$ \\
$\mathscr{S}_5=[-5,5~;~-5[\cup ]-3~;~-1[\cup ]4,5~;~5]$ \\
$\mathscr{S}_6=[-5,5~;~-4[\cup ]4,5~;~5]$
 \item $\bullet~$ Si $k<-3,5$, l'équation $f(x)=k$ n'a pas de solution.\\
 $\bullet~$ Si $k=-3,5$, l'équation $f(x)=k$ a une solution.\\
  $\bullet~$ Si $-3,5<k<0$, l'équation $f(x)=k$ a ....\\
  $\bullet~$ ....
\end{enumerate}

