\begin{enumerate}
\item \begin{enumerate}
  \item $S=\{-3~;~-1~;~3\}$
  \item  $S=\emptyset$
  \item $S=]-3~;~-1[\cup]3~;~7]$
\item $S=[3,5~;~7]\cup\{-2\}$
\item $S=]4~;~6[$
\item $S=[-4~;~4]\cup[6~;~7]$
\end{enumerate}
\item \begin{tikzpicture}
  \tkzTabInit[lgt=1,espcl=1.5]{$x$/0.5,\footnotesize $f(x)  $/0.5}{$-4$,$4$,$6$,$7$}
 \tkzTabLine{,+,z,-,z,+}
 \end{tikzpicture}
\end{enumerate}
