\begin{enumerate}
  \item Tableau de signes de la fonction $f$ :
  \begin{center}
\begin{tikzpicture}
  \tkzTabInit[lgt=2.5,espcl=1.3]{$x$/1,\footnotesize Signe de $ f(x)  $/1}{$-4$,$-3$,1,$4$}
 \tkzTabLine{,-,z,+,z,+}
    %\tkzTabIma{1}{3}{2}{0}
\end{tikzpicture}
      \end{center}


  \item Tableau de signes de la fonction $g$ :
  \begin{center}
\begin{tikzpicture}
  \tkzTabInit[lgt=2.5,espcl=1.3]{$x$/1,\footnotesize Signe de $ g(x)  $/1}{$-50$,$-40$,$-10$,$40$}
 \tkzTabLine{,+,z,-,z,+}
    %\tkzTabIma{1}{3}{2}{0}
\end{tikzpicture}
      \end{center}

  \item Tableau de signes de la fonction $h$ :
  \begin{center}
\begin{tikzpicture}
  \tkzTabInit[lgt=2.5,espcl=1.3]{$x$/1,\footnotesize Signe de $ h(x)  $/1}{$0$,$1$,$7$,$8$}
 \tkzTabLine{,-,z,+,z,+}
    %\tkzTabIma{1}{3}{2}{0}
\end{tikzpicture}
      \end{center}
  \item Tableau de signes de la fonction $u$ :
  \begin{center}
\begin{tikzpicture}
  \tkzTabInit[lgt=2.5,espcl=1.3]{$x$/1,\footnotesize Signe de $ u(x)  $/1}{$-0{,}8$,$-0{,}2$,$1$}
 \tkzTabLine{,+,z,-}
    %\tkzTabIma{1}{3}{2}{0}
\end{tikzpicture}
      \end{center}
\end{enumerate}
