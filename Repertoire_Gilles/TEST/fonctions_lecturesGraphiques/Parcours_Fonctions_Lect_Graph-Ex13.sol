\begin{center}
\textbf{-Partie A-}
\end{center}
\begin{enumerate}
\item $I=[-2,5~;~2,25]$.
 \item \begin{enumerate}
  \item $f(-1)=3$ et $g(-1)=-3$.
  \item Les antécédents de $-1$ par $g$ sont $-1,75$ et $1,75$.
  \item  $f(1,5)<g(1,5)$. Nabolos a donc tort.
   \end{enumerate}
\end{enumerate}
\begin{center}
\textbf{-Partie B-}
\end{center}
\begin{enumerate}
  \item $f(x)=0$\hspace{0.5cm}$\mathscr{S}=\{-2~;~0~;~2\}$.
  \item $g(x)<0$\hspace{0.5cm}$\mathscr{S}=]-2~;~2[$.
  \item $g(x)= -1$\hspace{0.5cm}$\mathscr{S}=\{-1,75~;~1,75\}$.
  \item $g(x)>-3,5$, $\mathscr{S}=[-2,5~;~-0,75[\cup ]0,75~;~2,25]$.
   \item $f(x)=g(x)$\hspace{0.5cm}$\mathscr{S}=\{-2~;~1~;~2\}$.
  \item $f(x)>g(x)$\hspace{0.5cm}$\mathscr{S}=]-2~;~1[\cup]2~;~2,25]$.
\end{enumerate}
