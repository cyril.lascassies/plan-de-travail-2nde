\begin{enumerate}
   \item \begin{enumerate}
           \item    Elle utilise bien tout le grillage.
           \item L'aire de l'enclos est donnée par : $OC\times OE$.
         \end{enumerate}
   \item \begin{enumerate}
           \item Comme la longueur du grillage est 50 m, on obtient l'égalité :$$y+(x+6)+(y+4)+x=50$$
           Soit $y=20-x$.
           \item L'aire de l'enclos est alors donnée par : $(x+6)(y+4)=\ldots$.
         \end{enumerate}
  \item   L'aire de l'enclos est maximale lorsque $x=9$. La largeur de l'enclos est $x+6=15$ m et sa longueur est $y+4=20-x+4=15$ m.\\
 C'est un carré ! L'aire maximale est 225 m$^2$.
 \end{enumerate}
