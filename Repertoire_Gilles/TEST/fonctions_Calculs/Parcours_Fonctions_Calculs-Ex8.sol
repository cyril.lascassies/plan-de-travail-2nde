
\begin{enumerate}
\item $4$.
\item
\begin{enumerate}
\item $2$.
\item On ne peut pas trouver de résultat final négatif puisque celui-ci est un carré.
\end{enumerate}
\item
\begin{enumerate}
\item
C'est la fonction $ x \longmapsto x^2+10x+25$. 
\item C'est faux.
\end{enumerate} 
\item
\begin{enumerate}
\item Il y a donc deux solutions $0$ et $- 10$.
\item $0$ et $- 10$
\end{enumerate}
\end{enumerate}
