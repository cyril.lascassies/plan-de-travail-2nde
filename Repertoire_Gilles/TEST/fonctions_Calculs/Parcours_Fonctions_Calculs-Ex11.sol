\textbf{Partie 1}
\begin{enumerate}
\item Utilisez l'exemple donné (première ligne du tableau)
\item Le tableau 2 est une généralisation du tableau 1.
\item $-50x^2+500x+10000$
\end{enumerate}
\textbf{Partie 2}
 \begin{enumerate}
 \item  $R(8)=\nombre{10800}$.
 \item $R(5)=\nombre{11250}$.
\item $\nombre{10800}$
\item Réduction : 16,80 € et donc prix de la place : 3,20 €.
\item $\nombre{11300}$ €. Prix de la place 15 €.
\end{enumerate}
