\begin{enumerate}
\item $243$ cm$^3$.
\item Non.
\item  Le volume est donné par Longueur $\times$ Largeur $\times$ Hauteur, soit $\mathcal{V}=(15-2x)^2\times x$.
\item $]0~;~7,5[$
\item On utilise le menu Graph de la calculatrice.
\item  $x\in [0,51~;~5,34]$
\item Oui, par exemple $x=3$.
\end{enumerate}
