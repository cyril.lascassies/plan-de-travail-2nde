{\bf Partie A}
\begin{enumerate}
%\item\begin{enumerate}
\item $MB=4-x$
\item $x=2$
\item L'aire est donnée par Longueur $\times$ Largeur.
\item On utilise le menu Graph de la calculatrice.
\end{enumerate}
{\bf Partie B}
\begin{enumerate}
\item $\bullet$ pour $2$ cm$^2$ : il y a deux rectangles possibles.\\
$\bullet$ pour $4$ cm$^2$ : il y a un rectangle possible.\\
$\bullet$ pour $5$ cm$^2$ : aucun rectangle possible.\\
\item Il faut développer les deux membres.
\item Résoudre $x(4-x)=3$ revient à résoudre ...
\end{enumerate}
