\begin{enumerate}
\item
\begin{enumerate}
\item %Si un tel rectangle a pour longueur 10 cm, quelle est sa largeur ?
Si $\ell$ est la largeur on a :

$\ell = 5,5$~cm.
\item
Si la longueur a pour mesure 13~cm, on a : $\ell = 2,5$~cm. 
\item  $\ell = 15,5 - x$.
\item  $\mathcal{A}(x)  = 15,5x - x^2$.
\end{enumerate}
\item
\begin{enumerate}
\item
$f(4) = 46$~cm$^2$.
\item $f(5) = 52,5$~cm$^2$.
\end{enumerate}
\item
%

\begin{enumerate}
\item À peu près 38.
\item À  peu près 3,3 et 12,2. 
\item On lit un peu plus de 60~cm$^2$ pour $x \approx 7,75$.
\end{enumerate}
\item Si $x = 7,75$ alors l'autre côté mesure $7,75$ donc  le rectangle est ...
\end{enumerate}

