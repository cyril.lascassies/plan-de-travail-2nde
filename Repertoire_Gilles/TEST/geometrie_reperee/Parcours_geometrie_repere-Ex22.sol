Les points $A$ et $B$ sont deux points de $\mathscr{C}_f$. Puisque la fonction est la fonction qui élève au carré,  leurs ordonnées sont les carrés de leurs abscisses. Vous avez donc les coordonnées des deux points $A$ et $B$ qui vous permettent de déterminer les coordonnées de leur milieu $M$. \\
La distance cherchée est $1$.
