\begin{enumerate}
    \item Les axes doivent être perpendiculaires et les unités de
      longueurs doivent être les mêmes.
    \item $A(-1;2)$.
      \setcounter{enumi}{3}
    \item
      \begin{enumerate}
        \setcounter{enumii}{1}
      \item $C(1;1)$.
      \end{enumerate}
    \item $K(0;1,5)$.
    \item
      \begin{enumerate}
      \item $E(3;-2)$ et $F(0;-3)$.
      \item Comme $E$ est le symétrique de $A$ par rapport à $I$ alors
        $I$ est le milieu du segment $[AE]$. Comme $F$ est le
        symétrique de $B$ par rapport à $I$ alors $I$ est le milieu du
        segment $[FB]$. Comme les diagonales du quadrilatère $ABEF$
        ont le même milieu alors $ABEF$ est un parallélogramme.
      \item C'est $B$.
      \end{enumerate}
  \end{enumerate}
