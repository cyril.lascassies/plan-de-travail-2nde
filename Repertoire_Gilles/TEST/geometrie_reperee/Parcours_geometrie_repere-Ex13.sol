\begin{enumerate}
\item Faites un repère orthonormé.
\item Montrez que le triangle $AHB$ est un triangle rectangle. Pour cela il faut calculer des longueurs et utiliser un théorème bien connu.
\item La droite $(AH)$ est une droite remarquable dans le triangle $ABC$. Mais laquelle ?
\end{enumerate}
