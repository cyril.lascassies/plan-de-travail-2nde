 \begin{enumerate}
  \item $A(-2;1)$; $B(1;2)$; $C(0;-2)$
  \item $A\left(-1;1,5\right)$; $B\left(\dfrac{1}{3};-2\right)$; $C\left(\dfrac{5}{3};1\right)$
  \item $A\left(-1;1,2\right)$; $B\left(-2;-0,6\right)$; $C\left(2;0\right)$
  \item $A(3;1)$; $B(-2;4)$; $C(-1;1)$
 \end{enumerate}

