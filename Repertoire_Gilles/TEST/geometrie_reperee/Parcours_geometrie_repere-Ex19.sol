\begin{enumerate}
   \item Le repère doit être orthonormé.
   \item $NE=4$, $NZ=\sqrt{80}$ et $EZ=8$.
   \item Utilisez la réciproque du théorème du théorème de Pythagore..
   \item  $K(0,4;3,2)$.
   \item $A$ est le symétrique de $E$ par rapport à $K$.
   \begin{enumerate}
      \item $K$ est le milieu de $[EA]$.
          \item  Justifier que $NAZE$ est un parallélogramme, puis que c'est un rectangle (utilisez les questions précédentes).
          \item Aire($NAZE$)$=32$.
          \item L'aire du triangle $NEZ$ est la moitié de celle du rectangle $NAZE$.
         \end{enumerate}
\item
\begin{enumerate}
 \item Le point $M$ est à l'intérieur du rectangle $NAZE$.
 \item  $[EM]$ est une hauteur du triangle $NEZ$. Son aire est donc donnée par $\dfrac{EM\times NZ}{2}=2\sqrt{5}EM$. Et comme l'aire de ce triangle est $16$, on peut en déduire que $2\sqrt{5}EM=32$ et obtenir la valeur exacte de $EM$.
 \item $NM=\dfrac{4}{\sqrt{5}}$.
 \item  $MZ=\dfrac{16}{\sqrt{5}}$.
\end{enumerate}
  \end{enumerate}
