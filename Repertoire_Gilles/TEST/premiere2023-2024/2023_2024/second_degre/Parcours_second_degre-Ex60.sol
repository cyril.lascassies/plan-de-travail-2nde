Donnez les coordonnées de $M$ en fonction de $a$ et $b$ ($M$ est sur la droite $(AB)$). On trouve $M(x;(b+a)x-ab)$.\\
Les coordonnées de $N$ sont $(x;x^2)$. \\
La distance $MN$ est $(a+b)x-ab-x^2$. Il ne reste plus qu'à étudier cette fonction.
