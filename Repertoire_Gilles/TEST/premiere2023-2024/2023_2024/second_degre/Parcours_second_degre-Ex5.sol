 \begin{colenumerate}
   \item $(x-3)(x+3)$
   \item $(x-9)(x-1)$
   \item Pas factorisable
   \item $2x(2-2x)$
   \end{colenumerate}
