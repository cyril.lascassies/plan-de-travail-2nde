\begin{enumerate}
\item Oui. Calculez $y$ pour $x=5$.
\item Résolvez l'équation $y=0$. On trouve 12,07 m.
\item Résolvez l'inéquation $y\geqslant 1,02$. La balle a une hauteur supérieure ou égale à 1,02 m entre 1 m et 9 m du joueur.
\end{enumerate}
