\begin{enumerate}
\item \begin{tikzpicture}
  \tkzTabInit[lgt=1,espcl=2]{$x$/0.5,\footnotesize $f(x)  $/0.5}{$-\infty$,$-2$,$3$,$+\infty$}
 \tkzTabLine{,+,z,-,z,+,}
 \end{tikzpicture}
\item \begin{tikzpicture}
  \tkzTabInit[lgt=1,espcl=2]{$x$/0.5,\footnotesize $g(x)  $/0.5}{$-\infty$,$\frac{1}{3}$,$+\infty$}
 \tkzTabLine{,-,z,-,}
 \end{tikzpicture}
\item \begin{tikzpicture}
  \tkzTabInit[lgt=1,espcl=2]{$x$/0.5,\footnotesize $h(x)  $/0.5}{$-\infty$,$+\infty$}
 \tkzTabLine{,+,}
 \end{tikzpicture}
\end{enumerate}
