\begin{enumerate}

\item Tableau de variations de $f$ (à justifier) :
\begin{center}
\begin{tikzpicture}[scale=0.8]
  \tkzTabInit{$x$/1,$f(x)$/2}{$-\infty$,$-1$,$+\infty$}
    \tkzTabVar{+/,-/$-10$,+/$~$}
    %\tkzTabIma{1}{3}{2}{0}
\end{tikzpicture}
      \end{center}
      \item $f$ admet un minimum qui vaut ...
\end{enumerate}
