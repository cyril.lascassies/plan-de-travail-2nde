\begin{enumerate}
\item \begin{enumerate}
\item Le point $M$ appartient à la droite d'équation $y=2x+3$, donc ...
\item On a bien $5x^2+6x+5=\left(x+0,6\right)^2+3,2$.
\item $f$ est décroissante sur $]-\infty;-0,6]$ et croissante sur $[-0,6;+\infty[$, elle admet donc un minimum en $x_0=-0,6$.
\item $M_0$ a pour abscisse $-0,6$. De plus $M_0\in d$, donc l'ordonnée de $M_0$ est égale à 1,8 (faites le calcul).
\end{enumerate}
\item
\begin{enumerate}
\item Calculez $2x+3$ pour $x=0$.
\item Le triangle est rectangle (réciproque du théorème de Pythagore), les droites $(AM_0)$ et $d$ sont perpendiculaires.
\end{enumerate}
\end{enumerate}
