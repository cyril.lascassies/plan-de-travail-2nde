 \begin{enumerate}
     \item $g(3)=-6$et $g(-1)=-2$.
     \item L'équation $g(x)=3$ est équivalente à l'équation $x^2-6x=0$. En factorisant $x^2-6x$ on se ramène à une équation produit nul.
   \end{enumerate}
