import { readFileSync, writeFileSync, unlinkSync } from "fs";
import { join, basename } from "path";
import { execSync } from "child_process";
import { createRequire } from "module";

const require = createRequire(import.meta.url);
const readline = require("readline");
const fs = require("fs");
const path = require("path");

const rootDir = "./PDT_Seconde";
const themes = require("./themes.json")

for (const theme in themes) {
  for (const sousTheme in themes[theme].sousThemes) {
    let dirPath = path.join(rootDir, themes[theme].sousThemes[sousTheme].url);
    const previewPath = path.join(dirPath, "preview");
    if (!fs.existsSync(previewPath)) {
      fs.mkdirSync(previewPath);
    }
    dirPath = path.join(dirPath, "tex");
    fs.readdir(dirPath, (err, files) => {
      if (err) {
        console.error(`Impossible de lire le répertoire ${dirPath}`);
        process.exit(1);
      }

      const texFiles = files.filter((file) => path.extname(file) === ".tex");

      for (const file of texFiles) {
        const filePath = path.join(dirPath, file);
        console.log(`Compiling ${filePath}...`);

        const content = readFileSync(filePath, "utf8");
        const preamble = readFileSync("./bin/preambule.tex", "utf8");
        const environnementExercice = readFileSync(
          "./bin/environnementExercice.tex",
          "utf8"
        );
        const environnementExercice_corr = readFileSync(
          "./bin/environnementCorrection.tex",
          "utf8"
        );
        const tmpDirPath = path.join(dirPath, "tmp");
        if (!fs.existsSync(tmpDirPath)) {
          fs.mkdirSync(tmpDirPath);
        }
        const tmpFilePath = join(dirPath, "tmp", file);
        const tmpFilePathCorr = tmpFilePath.replace(".tex", "_corr.tex");
        let tmpFileContent = `\\documentclass[french,preview,border=5pt]{standalone}\n${preamble}\n\n${environnementExercice}\n\\begin{document}\n\n\\begin{Maquette}[Fiche]{}`;
        tmpFileContent += `${content}\n\\end{Maquette}\n\n\\end{document}\n`;
        writeFileSync(tmpFilePath, tmpFileContent);
        let tmpFileContentCorr = `\\documentclass[french,preview,border=5pt]{standalone}\n${preamble}\n\n${environnementExercice_corr}\n\\begin{document}\n\n\\begin{Maquette}[Fiche,CorrigeApres]{}`;
        tmpFileContentCorr += `${content}\n\\end{Maquette}\n\n\\end{document}\n`;
        const regex = /\\begin\{Solution\}\[[^\]]*\]/g;
        tmpFileContentCorr = tmpFileContentCorr.replace(
          regex,
          "\\begin{Solution}"
        );
        writeFileSync(tmpFilePathCorr, tmpFileContentCorr);
        try {
          execSync(
            `lualatex -output-directory=${previewPath} ${tmpFilePath}  -jobname=${basename(
              filePath,
              ".tex"
            )} -interaction=nonstopmode -file-line-error -halt-on-error`
          );
          console.log(`Compiling ${tmpFilePathCorr}...`);
          execSync(
            `lualatex -output-directory=${previewPath} ${tmpFilePathCorr}  -jobname=${basename(
              filePath + "_corr",
              ".tex"
            )} -interaction=nonstopmode -file-line-error -halt-on-error`
          );
          const pdfPath = join(
            previewPath,
            `${basename(filePath, ".tex")}.pdf`
          );
          const pdfPathCorr = pdfPath.replace(".pdf", "_corr.pdf");
          execSync(
            `convert -density 300 -background white -flatten ${pdfPath} -quality 90 ${pdfPath.replace(
              ".pdf",
              ".png"
            )}`
          );
          execSync(
            `convert -density 300 -background white -flatten ${pdfPathCorr} -quality 90 ${pdfPathCorr.replace(
              ".pdf",
              ".png"
            )}`
          );
          unlinkSync(pdfPath.replace(".pdf", ".aux"));
          unlinkSync(pdfPath.replace(".pdf", ".log"));
          unlinkSync(pdfPath.replace(".pdf", ".out"));
          unlinkSync(pdfPathCorr.replace(".pdf", ".aux"));
          unlinkSync(pdfPathCorr.replace(".pdf", ".log"));
          unlinkSync(pdfPathCorr.replace(".pdf", ".out"));
        } catch (error) {
          console.log(`${tmpFilePath} n'a pas pu être compilé.`);
          console.log(error);
        }
      }
    });
  }
}
