import { readFileSync, writeFileSync, unlinkSync } from "fs";
import { join, basename } from "path";
import { execSync } from "child_process";
import { createRequire } from "module";

const require = createRequire(import.meta.url);
const readline = require("readline");
const fs = require("fs");
const path = require("path");

const rootDir = "./PDT_Seconde";
const outputDir = "static/2nd/";
const themes = require("./themes.json");

const referentiel = { PDT2nde: {} };

for (const theme in themes) {
  if (referentiel.PDT2nde[theme] === undefined) {
    referentiel.PDT2nde[theme] = {};
  }
  for (const sousTheme in themes[theme].sousThemes) {
    if (referentiel.PDT2nde[theme][sousTheme] === undefined) {
      referentiel.PDT2nde[theme][sousTheme] = {};
    }
    let dirPath = path.join(rootDir, themes[theme].sousThemes[sousTheme].url);
    dirPath = path.join(dirPath, "tex");
    await fs.readdir(dirPath, (err, files) => {
      if (err) {
        console.error(`Impossible de lire le répertoire ${dirPath}`);
      }
      const texFiles = files.filter((file) => path.extname(file) === ".tex");
      for (const file of texFiles) {
        const uuid = "2nd_" + path.basename(file, ".tex");
        const obj = {
          uuid,
          url: outputDir + file,
          png:
            outputDir +
            file.replace(".tex", ".png").replace("/tex/", "/preview/"),
          pngCor:
            outputDir +
            file.replace(".tex", "_corr.png").replace("/tex/", "/preview/"),
        };
        referentiel.PDT2nde[theme][sousTheme][uuid] = obj;
      }
    });
  }
}

// Wait 4s
await new Promise((resolve) => setTimeout(resolve, 1000));
fs.writeFile(
  "./bin/referentiel.json",
  JSON.stringify(referentiel, null, 2),
  "utf8",
  (error) => {
    if (error) {
      console.error(`Erreur: ${error}`);
      return;
    }
    console.log("Référentiel mis à jour");
  }
);

// async function getListOfFiles(dir) {
//   try {
//     const files = await fs.readdir(dir, { withFileTypes: true })
//     for (const file of files) {
//       const filePath = path.join(dir, file.name)
//       if (file.isDirectory() && !file.path.includes('node_modules') && !file.path.includes('Repertoire')) {
//         await getListOfFiles(filePath)
//       } else if (path.extname(filePath) === '.tex') {
//         texFiles.push(filePath)
//       }
//     }
//   } catch (err) {
//     console.error(`Impossible de lire le répertoire ${root}`)
//   }
// }

// await getListOfFiles(root);
// // Read emptyReferentiel.json
// const emptyReferentielFile = await fs.readFile('./bin/emptyReferentiel.json', 'utf8')
// const referentiel = JSON.parse(emptyReferentielFile)

// for (const theme in referentiel.PDT2nde) {
//   for (const sousTheme in referentiel.PDT2nde[theme]) {
//     for (const file of texFiles) {
//       if (file.includes('/' + sousTheme + '/')) {
//         const uuid = '2nd_' + path.basename(file, '.tex')
//         const obj = {
//           uuid,
//           url: outputDir + file,
//           png: outputDir + file.replace('.tex', '.png').replace('/tex/', '/preview/'),
//           pngCor: outputDir + file.replace('.tex', '_corr.png').replace('/tex/', '/preview/')
//         }
//         referentiel.PDT2nde[theme][sousTheme][uuid] = obj
//       }
//     }
//   }
// }
