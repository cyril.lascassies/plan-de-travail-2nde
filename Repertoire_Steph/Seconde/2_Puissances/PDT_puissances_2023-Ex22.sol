1.
$$
\begin{aligned}
2 \times 1 u+16 u & =18 u \\
& =18 \times 1,66054 \times 10^{-27} \mathrm{~kg} \\
& =2,988972 \times 10^{-26}
\end{aligned}
$$
2.
$$
\begin{aligned}
& \frac{1}{2,988972 \times 10^{-26}} \\
& =0,3345631876 \times 10^{26} \\
& \approx 3,35 \times 10^{25} \text { molécules d'eau } \\
& \text { 3. } 1,37 \times 10^9 \times 10^{12} \times 3,35 \times 10^{25} \\
& \approx 4,6 \times 10^{46} \text { molécules d'eau }
\end{aligned}
$$
4.Par seconde :
$$
\begin{aligned}
& 250 \times 10^3 \times 3,35 \times 10^{25} \\
& =8,375 \times 10^{30} \text { molécules d'eau }
\end{aligned}
$$
Par an :
$2,64 \times 10^{38}$ molécules d'eau
